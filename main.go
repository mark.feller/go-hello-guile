package main

/*
#cgo CFLAGS: -I/usr/local/Cellar/guile/2.2.0/include/guile/2.2 -I/usr/local/opt/gmp/include -I/usr/local/opt/readline/include -I/usr/local/Cellar/bdw-gc/7.6.0/include
#cgo LDFLAGS: -L/usr/local/Cellar/guile/2.2.0/lib -L/usr/local/Cellar/bdw-gc/7.6.0/lib -lguile-2.2 -lgc

#include <libguile.h>
extern SCM hello(SCM arg);
extern SCM square(SCM arg);
*/
import "C"

import (
	"fmt"
	"unsafe"
)

func main() {
	C.scm_init_guile()
	C.scm_c_primitive_load(C.CString("script.scm"))

	var fn C.SCM

	fn = C.scm_variable_ref(C.scm_c_lookup(C.CString("hello")))
	C.scm_call_0(fn)

	C.scm_c_define_gsubr(C.CString("go-hello"), 0, 0, 0, unsafe.Pointer(C.hello))
	fn = C.scm_variable_ref(C.scm_c_lookup(C.CString("go-hello")))
	C.scm_call_0(fn)

	C.scm_c_define_gsubr(C.CString("go-square"), 1, 0, 0, unsafe.Pointer(C.square))
	fn = C.scm_variable_ref(C.scm_c_lookup(C.CString("square")))
	C.scm_call_0(fn)
}

//export hello
func hello(arg C.SCM) C.SCM {
	n, _ := fmt.Println("Hello Go World!")
	return C.scm_from_int(C.scm_t_int32(n))
}

//export square
func square(arg C.SCM) C.SCM {
	// convert scheme to go int
	i := int(C.scm_to_int(arg))
	sq := i * i

	// convert go int back to scheme
	c_s := C.scm_t_int32(sq)
	return C.scm_from_int(c_s)
}
