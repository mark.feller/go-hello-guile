(define (hello)
  (define name "World")
  (display (string-append "Hello Guile " name "!"))
  (newline))

(define (hello-go)
  (go-hello))

(define (square)
  (display "Square of 3: ")
  (display (go-square 3))
  (newline))
